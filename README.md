# Brick Breakers Mobile


Game made in **Unreal Engine 5** based on the [Brick Breakers Tutorial](https://dev.epicgames.com/community/learning/courses/53d/unreal-engine-let-s-make-a-game-brick-breakers/KJjd/unreal-engine-first-things-first), ported to Mobile.
